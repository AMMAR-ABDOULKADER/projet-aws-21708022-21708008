// Les variables utilisée**********************************************************************************************
var div = document.querySelector('#calendar');
var n = document.getElementById("next");
var p = document.getElementById("prev");
let tab = [];
var dete=document.querySelector('#dete');
var today=document.querySelector('#today');
var modal = document.getElementById('myModal');
var modalu = document.getElementById('Modalupdate');
var modals = document.getElementById('Modalsup');
var dec = document.getElementById('deconnexion');
var con = document.getElementById('connexion');
var user = document.getElementById('user');
var type = document.getElementById('typ');
var nv = document.getElementById('nvadmin');
var al= document.getElementById('alert');
let tabi;
let tabj;
var jour = new Array("Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi");
var moi = new Array("Jan", "Fév", "Mars", "Avr", "Mai", "Juin", "Jui", "Août", "Sept", "Oct", "Nov", "Déc");
var lengt = document.getElementById("eventi").rows.length;
var liste = document.getElementById("liste");
var listall = document.getElementById('toutliste');
var d = new Date;
var tod = new Date;
d.setDate(d.getDate()-d.getDay());

let hr = new Date;
let hf = new Date;
    hr.setHours(0);
hr.setMinutes(0);
hf.setHours(0);
hf.setMinutes(30);



//creation du calendrier*********************************************************************************************


today.innerHTML="Aujourd'hui : "+jour[hr.getDay()]+" "+pad(hr.getDate())+" "+moi[hr.getMonth()]+" "+hr.getFullYear();

function calendrier(d,div){ 
    
    
	div.innerHTML='';
let table=document.createElement('table');
 let par=div.appendChild(table);


  
for ( i = 0 ; i < 49 ; i++) {
      tab[i]=[]
   
       tr= document.createElement('tr');
	   par.appendChild(tr);
      for ( j = 0 ; j < 8 ; j++) {
        let td=document.createElement('td');

        tab[i][j]=td;
		tr.appendChild(td);
        let nd = new Date(d);

        if (j==0 && i!=0)
        {      

          var nH = pad(hr.getHours());
          var nM = pad(hr.getMinutes());
              var fH = pad(hf.getHours());
          var fM = pad(hf.getMinutes());
          if(nM==0){
            hr.setMinutes(30);
          hf.setMinutes(60);}
            
          else{
            hr.setMinutes(60);
			hf.setMinutes(30);
          }
		  
       
    
          tab[i][j].innerHTML = nH+':'+nM;
		  tab[i][j].className='heure'
		  tab[i][j].setAttribute("datedeb",nH+":"+nM+":00");
		  tab[i][j].setAttribute("datefin",fH+":"+fM+":00");
            if(i==48){
                
                
                 tab[i][j].setAttribute("datefin",nH+":"+"59"+":00");
            }
		  
        }
        else if (i==0 && j != 0)
        {
             tab[i][j].className='date'
          nd.setDate(d.getDate()+(j-1));
          nd.toLocaleDateString();
          var ch = d.getDay()+(j-1);
          if (ch>=7){
              ch=ch-7;
          }
          var jo = pad(nd.getDate());
          var n = jour[ch];
          tab[i][j].innerHTML = ''+n+' '+jo;
		  tab[i][j].setAttribute("date_debut",pad(nd.getFullYear())+"-"+pad(nd.getMonth()+1)+"-"+pad(nd.getDate()));
             tab[i][j].setAttribute("dat",pad(nd.getDate()));
           
        }
        else{
			tab[i][j].className='evenement';
          tab[i][j].addEventListener("click", action);
          tab[i][j].setAttribute("lesdates",tab[0][j].getAttribute("date_debut"));
            tab[i][j].setAttribute("existe",0);
            tab[i][j].setAttribute("heures",tab[i][0].getAttribute("datedeb"));
            tab[i][j].setAttribute("heure",tab[i][0].getAttribute("datedeb"));
            if(tab[i][j].getAttribute("lesdates")==pad(tod.getFullYear())+"-"+pad(tod.getMonth()+1)+"-"+pad(tod.getDate())){
                tab[i][j].style.background="yellow";
            }
            
           
        }
		 tab[i][j].dataset.column=j;
      tab[i][j].dataset.row=i; 
          if(j!=48){
	  tab[i][j].dataset.rows=i+1; 
          }
      }
	 
    }

  tab[0][0].className='zero';
    if(tab[0][1].getAttribute("dat")>24 && d.getMonth()!=11){
	dete.innerHTML=moi[d.getMonth()]+"-"+moi[d.getMonth()+1]+" "+d.getFullYear();
    }
    else if(tab[0][1].getAttribute("dat")>24 && d.getMonth()==11){
        dete.innerHTML=moi[d.getMonth()]+"-"+moi[0]+" "+d.getFullYear();
    }
    else {
        dete.innerHTML=moi[d.getMonth()]+" "+d.getFullYear();
    }

}
 
//deconnexion & parti admin********************************************************************************************
function deconnexion(){
if(user.value!=''){
	dec.style.display='block';
	con.style.display='none';
    liste.style.display='block';
    if(type.value=='admin'){
        listall.style.display='block';
        nvadmin.style.display='block';
    }
	
}
}

//action de clic******************************************************************************************************
function action(){
			
if(user.value!=''){
    let v=0;
     for(var i=1;i<lengt;i++)
  {  
				 var t = document.getElementById("eventi").rows[i].cells;
          
    
       if(tab[event.target.dataset.row][event.target.dataset.column].getAttribute("lesdates")==t[3].innerHTML && tab[event.target.dataset.row][event.target.dataset.column].getAttribute("heure")==t[4].innerHTML  && t[2].innerHTML==user.value)
      
     {
         
         tabi=event.target.dataset.row;
         tabj=event.target.dataset.column;
		modif_evenements(t);
           v=1;
         
     
          }
    
  }
    if (v==0){
    creer_evenement();
    }
			 
  			}else{
				al.style.display='block';
			}
			  }

//ferme les modal***************************************************************************************************
window.onclick = function(event) {
    if (event.target == modal || event.target == al || event.target == modalu || event.target == modals) {
      modal.style.display = "none";
	  al.style.display='none';
     modalu.style.display="none";
         modals.style.display="none";
       
	 
}}
 

// ajouter des zero devant les nombre inferieur a 10****************************************************************
function pad(number) { 
  return (number < 10 ? '0' : '') + number 
}
   

 

// action bouton next************************************************************************************************
n.onclick= nextWeek;
function nextWeek(){
	
	
	var res = d.setTime(d.getTime() + (7 * 24 * 60 * 60 * 1000));
	d = new Date(res);
	calendrier(d,div);
    evenements();
		
}

// action bouton prev**************************************************************************************************
p.onclick= prevWeek;
function prevWeek(){
	var res = d.setTime(d.getTime() - (7 * 24 * 60 * 60 * 1000));
	d = new Date(res);
	calendrier(d,div);
    evenements();
}

//Affiche le Modal pour creer un evenement*****************************************************************
function creer_evenement(){
    
    
    modal.style.display='block';
        
		  document.getElementById("datefin").value =tab[event.target.dataset.row][0].getAttribute("datefin") ; 
		  document.getElementById("datedeb").value =tab[event.target.dataset.row][0].getAttribute("datedeb") ; 
		document.getElementById("data").value =tab[0][event.target.dataset.column].getAttribute("date_debut") ;
		
    
}


// affiche les evenement dans le calendrier****************************************************************************
function evenements()
{ 
    
  for(var i=1;i<lengt;i++)
  {  
            var t = document.getElementById("eventi").rows[i].cells;
     for(var j=1;j<49;j++){

     for(var k=1;k<8;k++){
    
       if(tab[j][k].getAttribute("lesdates")==t[3].innerHTML && tab[j][k].getAttribute("heures")==t[4].innerHTML  && t[2].innerHTML==user.value)
     {
         
		// alert('salut');
          tab[j][k].innerHTML=t[1].innerHTML;
         tab[j][k].setAttribute("existe",1);
         tab[j][k].style.background='gray';
         tab[j][k].style.border='gray solid';
          tab[j][k].style.color='white';
         
         
         for(var m = j+1;m<48;m++){
         
          if(tab[m][k].getAttribute("heures")<t[5].innerHTML){
          if( tab[m][k].getAttribute("existe")==0 ){
              
              tab[m][k].style.background='gray';
              tab[m][k].setAttribute("existe",1);
            
                  tab[m][k].setAttribute("heure",t[4].innerHTML);
             
          }
              else if ( tab[m][k].getAttribute("existe")==1 ){
                  tab[m][k].style.background='red';
                  tab[m][k].style.border='red solid'
                 
              }
            
          
          }
     }
     }
     }
  
}
}
}

//Affiche et rempli le Modal de modififcation d'un evenement***********************************************************
function modif_evenements(t)
{ 
     modalu.style.display='block';
    document.getElementById("id").value =t[0].innerHTML;
        document.getElementById("titre").value =t[1].innerHTML ;
		  document.getElementById("dattefin").value =t[5].innerHTML; 
		  document.getElementById("dattedeb").value =t[4].innerHTML ; 
		document.getElementById("datta").value =t[3].innerHTML ;
	
        }


//Affiche et rempli le Modal de supression d'un evenement*************************************************************
function sup_even()
{ 
     modalu.style.display='none';
    modals.style.display='block';
    
       for(var i=1;i<lengt;i++)
  {  
				 var t = document.getElementById("eventi").rows[i].cells;
     
    
if(tab[tabi][tabj].getAttribute("lesdates")==t[3].innerHTML && tab[tabi][tabj].getAttribute("heure")==t[4].innerHTML  && t[2].innerHTML==user.value){
    document.getElementById("idd").value =t[0].innerHTML;
        document.getElementById("ttitre").value =t[1].innerHTML ;
		  document.getElementById("datttefin").value =t[5].innerHTML ; 
		  document.getElementById("datttedeb").value =t[4].innerHTML; 
		document.getElementById("dattta").value =t[3].innerHTML ;
		
    document.getElementById("llignn").value =t[6].innerHTML; 
    document.getElementById("ccoll").value =t[7].innerHTML ; 
 
}
  }

}



 
calendrier(d,div);
deconnexion();
evenements();



