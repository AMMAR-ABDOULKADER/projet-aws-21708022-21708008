var knex = require('knex')({
  client: 'mysql',
  connection: {
    host : 'localhost',
    user : 'root',
    password : '',
    database : 'calendar'
  }
});

async function init_utilisateur() 
{
  await knex.schema.dropTableIfExists('utilisateur');
  
  await knex.raw(`CREATE TABLE utilisateur(
  login VARCHAR(255) PRIMARY KEY NOT NULL,
  passe VARCHAR(30) NOT NULL,
  name VARCHAR(255) NOT NULL,
  type VARCHAR(255) NOT NULL,
  last_name VARCHAR(255) NOT NULL,
  dayofbirth VARCHAR(255) NOT NULL,
  modif VARCHAR(255) NOT NULL,
  delet VARCHAR(255) NOT NULL,
  ajout VARCHAR(255) NOT NULL
)`);
    
    var user={
		
		login : "admin",
		passe : "1234",
		name : "adminis",
        type :"admin",
		last_name : "ad",
		dayofbirth : "12/09/1995",
        modif :"true",
		delet : "true",
		ajout : "true"
	
	}
    
    await knex('utilisateur').insert(user);

await knex.destroy();
}

async function init_evenement() 
{
  await knex.schema.dropTableIfExists('evenement');
  
  await knex.raw(`CREATE TABLE evenement(
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  titre VARCHAR(255),
  user VARCHAR(255) ,
  heure_deb TIME ,
  heure_fin TIME ,
  date VARCHAR(255) ,

  FOREIGN KEY (user) REFERENCES users(login)

)`);

await knex.destroy();
}

init_utilisateur();
init_evenement();