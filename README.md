=========================

Dans ce projet nous avons r�alis� un calendrier de gestion d'ev�nements par semaine.
La structure de notre projet est la suivante:

--->Une base de donn�es:database.js 
Elle contient les diff�rentes tables dont on a besoin pour notre projet, une table pour les utilisateurs et creer un administrateur par defaut login:admin, mot de passe: 1234 et une table pour les �v�nements .

--->Un serveur:server.js 
Il contient certaines configuratios ainsi que nos routes decoup�es en gestionnaires:

- Route / repr�sente notre page principale le calendrier .
- Route /loginadmin: permettant de se connecter en temp qu'admin. Redirige vers / apr�s un login r�ussi.
- Route /login: permettant de se connecter en tand qu'invite. Redirige vers / apr�s un login r�ussi.
- Route /logout: permettant de se d�connecter. Redirige vers / apr�s un logout r�ussi.
- Route /ajoute: permettant d�ajouter un �v�nement. 
- Route /liste: permettant d�afficher la liste des �v�nements de l'utilisateur connecter.
- Route /listetout: permettant d�afficher la liste de tout les �v�nements des utilisateurs pour l'administrateur.
- Route /user: permettant d�afficher la liste des utilisateurs a l'administrateur.
- Route /update : permettant de modifier un evenement dans le calendrier.
Redirige vers /.
- Route /updatee : permettant de modifier un evenement dans la liste des evenements d'un utilisateur.
Redirige vers /liste.
- Route /updatetout : permettant de modifier un evenement dans la liste de tout les evenement des utilisateur vue par l'admin.
Redirige vers /listetout.
- Route /updatuser : permettant de modifier le droit d'utilisation d'un user par l'admin .
- Route /delete : permettant de supprimer un evenement dans le calendrier.
Redirige vers /.
- Route /deletee : permettant de supprimer un evenement dans la liste des evenements d'un utilisateur.
Redirige vers /liste.
- Route /deletetout : permettant de supprimer un evenement dans la liste de tout les evenement des utilisateur vue par l'admin.
Redirige vers /listetout.
- Route /deleteuser : permettant de supprimer un user dans la liste des utilisateur de l'admin.
Redirige vers /user.
- Route /nouveau_admin: permettant de se diriger vers le formulaire de creation d'un nouveau admin et creer.
- Route /signin: permettant de se diriger vers le formulaire de creation d'un nouveau invite et creer.



---> Une page d'inscription des invit�:formaccount.html
---> Une page d'inscription des administrateur:accountadmin.html
---> Une page de connexion:index1.html
---> Une page principale:index.html
---> Une page de liste des �v�nements:liste.html
---> Une page de liste des �v�nementsde tout les utilisateur :listetout.html
---> Une page de liste des utilisateur:user.html
---> trois pages de gestion script.js,script1.js,script2.js dans lequel figure nos diff�rents fonction pour la gestion des interactions entre l'application et le client.
---> Une feuille de style CSS.
---> Un fichier package.json contenant les packages utilis�s.

========================

Les instructions pour lancer le projet:

Au lancement du projet y'a le calendrier vide qui s'affiche si on clique sur une case sans se connecter il nous demande de se connecter le bouton connexion nous redirige vers le formulaire de connexion on peut se connecter en tant qu'admin avec le login par defaut admin et mot de passe 1234 ou creer un nouveau compte en cliquant sur nouveau compte.

--->Connexion de l'invite:
Une fois connecter, on peut ajouter, modifier ou supprimer un evenement, voir la liste de ses evenements et se deconnecter avec le boutton deconnexion.

--->Connexion de l'administrateur:
Une fois connecter, on peut ajouter, modifier ou supprimer un evenement, voir la liste de ses evenements, la liste des evenements de tout les utilisateurs, on peut aussi ajouter des administrateurs, definir les droits d'acces des utilisateurs et se deconnecter avec le boutton deconnexion.