var express = require('express');
var app = express();
var bodyP = require('body-parser');
var session = require('express-session');
var nunjucks = require('nunjucks');

app.use(session( {
  secret : '12345',
  resave: false,
  saveUninitialized: false,
} ));

nunjucks.configure('views', {
    express: app
});

var knex = require('knex')({
  client: 'mysql',
  connection: {
    host : 'localhost',
    user : 'root',
    password : '',
    database : 'calendar'
  }
});

app.use('/public',express.static('public'))
app.use(bodyP.urlencoded({ extended: false }));
app.use(bodyP.json());



//chemin vers la page principale***************************************************************************************
app.post('/',async function(req, res) {
	
	var user = await knex('utilisateur').where({
    login: req.body.log,
    passe: req.body.mot_de_passe,
    
  }).first();
  if (user) {
    req.session.user = user;
      
       try {
    res.render('index.html', {evenn: await knex.select('*').from('evenement'), 'users' : req.session.user});
  }
   catch (err) {
      console.error(err);
      res.status(500).send('Error');
    }
  }else {
    res.render('index1.html', { 
      login: req.body.log,
      message: ' login ou mot de passe erreur',
    });
  
  }
});

app.get('/',async function(req, res) {
    
    if (req.session.user) { 
  
    try {
        res.render('index.html', {evenn: await knex.select('*').from('evenement'),'users' : req.session.user});
    } catch (err) {
      console.error(err);
      res.status(500).send('Error');
    }
  }else {
   res.render('index.html');
  }
 
});


// chemins vers la connexion et l'inscription************************************************************************** 
app.get('/login', function(req, res) {
	
	res.render('index1.html');
});

app.post('/login',async function(req, res) {
	var user={
		
		login : req.body.log,
		passe : req.body.passe,
		name : req.body.nom,
        type : req.body.type,
		last_name : req.body.prenom,
		dayofbirth : req.body.date_nais,
	      modif: "true",
        delet: "true",
        ajout: "true"
	}
	try{
		if(user.login && user.passe && user.name && user.last_name && user.dayofbirth&& user.type){
	await knex('utilisateur').insert(user);   
		res.render('index1.html', { user: user, messages: 'Enregistré' });
        }
		else{
			res.render('formaccount.html', { user: user, message: 'Remplissez tout les case' });
			
		}
		} catch (err) {
    if (err.code == 'SQLITE_CONSTRAINT') {
      response.render('formaccount.html', { user: user, message: 'utilisateur exists deja' });
    } else {
      console.error(err);
      response.status(500).send('Error');
    }
  }
});

//chemin pour la connexion et l'incription admin***********************************************************************

app.post('/loginadmin',async function(req, res) {
	var user={
		
		login : req.body.log,
		passe : req.body.passe,
		name : req.body.nom,
        type : req.body.type,
		last_name : req.body.prenom,
		dayofbirth : req.body.date_nais,
		 modif: "true",
        delet: "true",
        ajout: "true"
	
	}
	try{
		if(user.login && user.passe && user.name && user.last_name && user.dayofbirth&& user.type){
	await knex('utilisateur').insert(user);   
		res.redirect('/');}
		else{
			res.render('accountadmin.html', { user: user, message: 'Remplissez tout les case' });
			
		}
		} catch (err) {
    if (err.code == 'SQLITE_CONSTRAINT') {
      response.render('formaccount.html', { user: user, message: 'utilisateur exists deja' });
    } else {
      console.error(err);
      response.status(500).send('Error');
    }
  }
});


// chemin pour l'ajout des evenement***********************************************************************************
app.post('/ajoute',async function(req, res) {
	var evene={
		
		titre : req.body.titre,
		user : req.body.log,
		heure_deb : req.body.datedeb,
		heure_fin : req.body.datefin,
		date : req.body.date
        
	
	}
	try{
		if(req.session.user.ajout==="false"){
	  
		res.render('index.html', { evenn: await knex.select('*').from('evenement'), evene: evene, message: 'Vous n avez pas ce droit' ,'users' : req.session.user});
            
            
        }
		else if(evene.titre && evene.user && evene.heure_deb && evene.heure_fin && evene.date){
            
            if(evene.heure_fin < evene.heure_deb){
                res.render('index.html', { evenn: await knex.select('*').from('evenement'), evene: evene, message: "l'heure Fin est inferieur de l'heure de debut" ,'users' : req.session.user});
            }else{
	await knex('evenement').insert(evene);   
		res.render('index.html', { evene: evene, message: 'Ajout avec succes' ,'users' : req.session.user});
        }
        }
		else{
			res.render('index.html', { evene: evene, message: 'Remplissez tout les case' ,'users' : req.session.user});
			
		}
		}
    catch (err) {
    if (err.code == 'SQLITE_CONSTRAINT') {
      res.render('index.html', { evene: evene, message: 'evenement exists deja', 'users' : req.session.user });
    } else {
      console.error(err);
      response.status(500).send('Error');
    }
  }
        
});

app.get('/ajoute', function(req, res) {
res.redirect('/');
});


//chemin pour inscription admin***************************************************************************************
app.post('/nouveau_admin', function(req, res) {
 res.render('accountadmin.html', { 'users' : req.session.user});
});

app.get('/nouveau_admin', function(req, res) {
 res.render('accountadmin.html', { 'users' : req.session.user});
});


//chemin pour inscription invite
app.get('/signin', function(req, res) {
 res.sendFile(__dirname + '/views/formaccount.html');
});

//chemin pour la deconnexion*****************************************************************************************
app.get('/logout', (req, res) => {
  req.session.user = null;
  res.redirect('/');
});


//chemin pour l'affichage des evenements*******************************************************************************
app.get('/liste',async function(req, res) {
	
  
    try {
        res.render('liste.html', {evenn: await knex.select('*').from('evenement').where('user',req.session.user.login),'users' : req.session.user});
    } catch (err) {
      console.error(err);
      res.status(500).send('Error');
    }

 
});



app.post('/liste',async function(req, res) {
	
  
    try {
        res.render('liste.html', {evenn: await knex.select('*').from('evenement').where('user',req.session.user.login),'users' : req.session.user});
    } catch (err) {
      console.error(err);
      res.status(500).send('Error');
    }

 
});



app.get('/listetout',async function(req, res) {
	
  
    try {
        res.render('listetout.html', {evenn: await knex.select('*').from('evenement'),'users' : req.session.user});
    } catch (err) {
      console.error(err);
      res.status(500).send('Error');
    }

 
});
app.post('/listetout',async function(req, res) {
	
  
    try {
        res.render('listetout.html', {evenn: await knex.select('*').from('evenement'),'users' : req.session.user});
    } catch (err) {
      console.error(err);
      res.status(500).send('Error');
    }

 
});


app.get('/user',async function(req, res) {
	
  
    try {
        res.render('user.html', {useers: await knex.select('*').from('utilisateur').where('type','invite'),'users' : req.session.user});
    } catch (err) {
      console.error(err);
      res.status(500).send('Error');
    }

 
});
app.post('/user',async function(req, res) {
	
  
    try {
        res.render('user.html', {useers: await knex.select('*').from('utilisateur').where('type','invite'),'users' : req.session.user});
    } catch (err) {
      console.error(err);
      res.status(500).send('Error');
    }

 
});



// chemin pour la modification*****************************************************************************************
app.get('/update', function(req, res) {
 
res.redirect('/');
});

app.post('/update',async function(req, res) {
	var evenne={
		id : req.body.id,
		titre : req.body.titre,
		user : req.body.log,
        heure_deb : req.body.datedeb,
		heure_fin : req.body.datefin
        
	
	}
    
	try{
       
        
        if(req.session.user.modif==="false"){
	  
		res.render('index.html', { evenne: evenne, message: 'Vous n avez pas ce droit' ,'users' : req.session.user});
            
            
        }
		else if(evenne.titre && evenne.user ){
            if(evenne.heure_deb>evenne.heure_fin ||evenne.heure_deb==evenne.heure_fin  ){
                res.render('index.html', { evenne: evenne, message: 'Heure fin superieur de l heure debut' ,'users' : req.session.user});
            }
            else{
	await knex('evenement').where({ id:evenne.id}).update(evenne)   
		res.render('index.html', { evenne: evenne, message: 'Modification avec succes' ,'users' : req.session.user});
            
            
        }
        }
		else{
			res.render('index.html', { evenne: evenne, message: 'Remplissez tout les case' ,'users' : req.session.user});
			
		}
		}
    catch (err) {
      console.error(err);
      res.status(500).send('Error');
    }
        
});



app.get('/updatuser', function(req, res) {
 
res.redirect('/user');
});

app.post('/updatuser',async function(req, res) {
	
	try{
		if(req.body.modif){
	await knex('utilisateur').where({ login:req.body.log}).update({modif:req.body.mod})   
		res.render('user.html', { useers: await knex.select('*').from('utilisateur').where('type','invite'), message: 'Modification avec succes' ,'users' : req.session.user});
            
            
        }
        else if(req.body.dele){
	await knex('utilisateur').where({ login:req.body.log}).update({delet:req.body.del})   
		res.render('user.html', { useers: await knex.select('*').from('utilisateur').where('type','invite'), message: 'Modification avec succes' ,'users' : req.session.user});
            
            
        }else if(req.body.ajou){
	await knex('utilisateur').where({ login:req.body.log}).update({ajout:req.body.aj})   
		res.render('user.html', { useers: await knex.select('*').from('utilisateur').where('type','invite'), message: 'Modification avec succes' ,'users' : req.session.user});
            
         
        }else{
			res.render('user.html', {  useers: await knex.select('*').from('utilisateur').where('type','invite'),'users' : req.session.user});
			
		}
		}
    catch (err) {
      console.error(err);
      res.status(500).send('Error');
    }
        
});


app.get('/updatee', function(req, res) {
 
res.redirect("/liste");
       
});

app.post('/updatee',async function(req, res) {
	var evenne={
		id : req.body.id,
		titre : req.body.titre,
		user : req.body.log,
        heure_deb : req.body.datedeb,
		heure_fin : req.body.datefin
        
	
	}
	try{
        
      
		 if(req.session.user.modif==="false"){
	  
		res.render('liste.html', { evenne: evenne, message: 'Vous n avez pas ce droit' ,'users' : req.session.user});
            
            
        }
		else if(evenne.titre && evenne.user ){
            
            if(evenne.heure_deb>evenne.heure_fin ||evenne.heure_deb==evenne.heure_fin  ){
                res.render('liste.html', { evenne: evenne, message: 'Heure fin superieur de l heure debut' ,'users' : req.session.user});
            }
            else{
	await knex('evenement').where({ id:evenne.id}).update(evenne)   
		res.render('liste.html', { evenn: await knex.select('*').from('evenement'), message: 'Modification avec succes' ,'users' : req.session.user});
            
            }
        }
		else{
			res.render('liste.html', { evenne: evenne, message: 'Remplissez tout les case' ,'users' : req.session.user});
			
		}
		}
    catch (err) {
      console.error(err);
      res.status(500).send('Error');
    }
        
});


app.get('/updatetout', function(req, res) {
 
res.redirect("/listetout");
       
});

app.post('/updatetout',async function(req, res) {
	var evenne={
		id : req.body.id,
		titre : req.body.titre,
		user : req.body.log,
        heure_deb : req.body.datedeb,
		heure_fin : req.body.datefin
	
	}
	try{
        
		 if(evenne.titre && evenne.user ){
              if(evenne.heure_deb>evenne.heure_fin || evenne.heure_deb==evenne.heure_fin  ){
                  res.render('listetout.html', { evenne: evenne, message: 'Heure fin superieur de l heure debut' ,'users' : req.session.user});
              }
             else {
	await knex('evenement').where({ id:evenne.id}).update(evenne)   
		res.render('listetout.html', { evenn: await knex.select('*').from('evenement'), message: 'Modification avec succes' ,'users' : req.session.user});
              }
         
            
        }
		else{
			res.render('listetout.html', { evenne: evenne, messages: 'Remplissez tout les case' ,'users' : req.session.user});
			
		}
		}
    catch (err) {
      console.error(err);
      res.status(500).send('Error');
    }
        
});



// chemin pour la suppression*****************************************************************************************
app.get('/delete', function(req, res) {
 
res.redirect('/');
});

app.post('/delete',async function(req, res) {
	
	try{
		if(req.session.user.delet==="false"){
	  
		res.render('index.html', {  message: 'Vous n avez pas ce droit' ,'users' : req.session.user});
            
            
        }
		else if(req.body.id){
	await knex('evenement').where('id',req.body.id).del();   
		res.render('index.html', {message: 'Suppression avec succes' ,'users' : req.session.user});
        }
		else{
			res.render('index.html', { evenne: evenne, 'users' : req.session.user});
			
		}
		}
    catch (err) {
      console.error(err);
      res.status(500).send('Error');
    }
        
});


app.get('/deletee', function(req, res) {
 
res.redirect("/liste");
});

app.post('/deletee',async function(req, res) {
	
	try{
			if(req.session.user.delet==="false"){
	  
		res.render('liste.html', {  message: 'Vous n avez pas ce droit' ,'users' : req.session.user});
            
            
        }
		else if(req.body.id){
	await knex('evenement').where('id',req.body.id).del();   
		res.redirect("/liste");
        }
		else{
			res.render('liste.html', { evenne: evenne, 'users' : req.session.user});
			
		}
		}
    catch (err) {
      console.error(err);
      res.status(500).send('Error');
    }
        
});

app.get('/deleteeuser', function(req, res) {
 
res.redirect("/user");
});

app.post('/deleteeuser',async function(req, res) {
	
	try{
		if(req.body.id){
            console.log(req.body.log)
	await knex('utilisateur').where({'login':req.body.id}).del();   
		res.render('user.html', { useers: await knex.select('*').from('utilisateur').where('type','invite'), message: 'Suppression avec succes' ,'users' : req.session.user});
        }
		else{
			res.render('user.html', { useers: await knex.select('*').from('utilisateur').where('type','invite'),  message: 'Echec' ,'users' : req.session.user});
		}
		}
    catch (err) {
      console.error(err);
      res.status(500).send('Error');
    }
        
});






app.get('/deletetout', function(req, res) {
 
res.redirect("/listetout");
});

app.post('/deletetout',async function(req, res) {
	
    try{
	if(req.body.id){
	await knex('evenement').where('id',req.body.id).del();   
		res.redirect("/listetout");
        }
		else{
			res.render('listetout.html', { evenne: evenne, 'users' : req.session.user});
			
		}
		}
    catch (err) {
      console.error(err);
      res.status(500).send('Error');
    }
        
});


//*****************************************************************************************************************
var listener = app.listen(808, function () {
  console.log('Your app is listening on port ' + listener.address().port);
});
